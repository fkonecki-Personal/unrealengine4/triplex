# Triple X

Simple number guessing game, played through the terminal.

You play a secret agent breaking into a computer, trying to retrieve 3 secured files. Every file is secured with 3 secret numbers, that player has to guess with the help of provided hints. 

<img src="/Screenshots/Main.png" alt="Screenshot" width="550" height="400">

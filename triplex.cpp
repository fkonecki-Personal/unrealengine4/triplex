#include <iostream>
#include <ctime>

void PrintIntroduction() 
{
	std::cout << R"(
             ________________________________________________
            /                                                \
           |    _________________________________________     |
           |   |                                         |    |
           |   |  $ open SecretFiles                     |    |
           |   |  Enter code: _                          |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |_________________________________________|    |
           |                                                  |
            \_________________________________________________/
                   \___________________________________/
                ___________________________________________
             _-'    .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.  --- `-_
          _-'.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.  .-.-.`-_
       _-'.-.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-`__`. .-.-.-.`-_
    _-'.-.-.-.-. .-----.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-----. .-.-.-.-.`-_
 _-'.-.-.-.-.-. .---.-. .-------------------------. .-.---. .---.-.-.-.`-_
:-------------------------------------------------------------------------:
`---._.-------------------------------------------------------------._.---'      
)" << '\n';

	std::cout << "Welcome agent...\n";

}

bool PlayGame(int LevelDifficulty) 
{
	std::cout << "\nYou are breaking into a level " << LevelDifficulty << " secure server room.\n";
	std::cout << "Enter the correct code to continue.\n";

	int CodeA = rand() % (LevelDifficulty * 2) + 1;
	int CodeB = rand() % (LevelDifficulty * 2) + 1;
	int CodeC = rand() % (LevelDifficulty * 2) + 1;

	int CodeSum = CodeA + CodeB + CodeC;
	int CodeProduct = CodeA * CodeB * CodeC;

	std::cout << "+ There are 3 numbers in the code";
	std::cout << "\n+ Numbers add-up to: " << CodeSum;
	std::cout << "\n+ Numbers multiply to give: " << CodeProduct << "\n";

	int GuessA, GuessB, GuessC;

	std::cin >> GuessA >> GuessB >> GuessC;

	int GuessSum = GuessA + GuessB + GuessC;
	int GuessProduct = GuessA * GuessB * GuessC;

	if (GuessSum == CodeSum) 
    {
		std::cout << "Great job agent, you have successfully retrieved the file!\n";
	} 
    else 
    {
		std::cout << "\nBe careful agent, the code was incorrect! Code is resetting...\n";
	}

    return GuessSum == CodeSum;
}

int main() 
{
    srand(time(NULL));

	int LevelDifficulty = 1;
	int MaxDifficulty = 3;

	PrintIntroduction();

    while (LevelDifficulty <= MaxDifficulty) 
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);
        std::cin.clear(); // Clears any errors
        std::cin.ignore(); // Discards the buffer

        if (bLevelComplete) 
        {
            LevelDifficulty++;
        }
    }

    std::cout << "Mission complete. Now get out of there agent!";

    return 0;
}